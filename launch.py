import js_api
import webview
import random
import threading
import time
import json

def send_english_subtitle(window, subtitle):
    s = 'window.dispatchEvent(new CustomEvent("enSub", %s))' % json.dumps({'detail': {'subtitle': subtitle}})
    print(s)
    window.evaluate_js(s)

if __name__ == '__main__':
    contents = open('index.html').read()
    window = webview.create_window(
        'The title of my window',
        html = contents,
        js_api = js_api.JSAPI
    )
    time.sleep(2)
    def subtitles(window, interval):
        for _ in range(15):
            time.sleep(interval)
            send_english_subtitle(
                window,
                random.choice(['hi', 'hello', 'bye', 'cook', 'fox'])
            )
    th = threading.Thread(target = subtitles, args = (window, 4))
    th.start()
    window2 = webview.create_window(
        'The title of my window 2!',
        html = contents,
        js_api = js_api.JSAPI
    )
    th2 = threading.Thread(target = subtitles, args = (window2, 2))
    th2.start()
    webview.start(gui='mshtml')
