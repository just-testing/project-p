import random
import string
import time

class JSAPI(object):
    @classmethod
    def create_time_file(cls, contents):
        time_str = str(time.time())
        with open(time_str + '.timefile', "w") as fp:
            fp.write(contents)

    @classmethod
    def random_string(cls, n=10):
        return ''.join(
            random.choice(string.ascii_letters)
            for _ in range(n)
        )
